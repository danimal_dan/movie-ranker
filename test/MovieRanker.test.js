import "regenerator-runtime/runtime";
import { UserPreferences } from "../src/model/UserPreferences.js";
import api from "../src/MovieApi.js";
import movieRanker from "../src/MovieRanker.js";

test("rank movies - user 1 - likes the old stuff", async () => {
  const userPreferences = new UserPreferences({
    prefersOlderMovies: true,
    genreRatings: {
      Action: 1,
      Comedy: 5,
      Drama: 1,
      Crime: 4,
      Animation: 3,
      Family: 2,
      Horror: 2,
    },
    alreadyWatchedTitles: ["Big Fish", "The Big Short", "Big Daddy"],
  });

  const movies = await api.fetchMovies();
  const rankedMovies = movieRanker.rankMovies(movies, userPreferences);

  expect(rankedMovies.length).toBe(5);

  const movieTitlesInRanking = rankedMovies.map((movie) => movie.title);
  expect(movieTitlesInRanking).not.toEqual(
    expect.arrayContaining(userPreferences.alreadyWatchedTitles)
  );

  expect(rankedMovies[0].title).toBe("Big Lebowski");
  expect(rankedMovies[1].title).toBe("Pee-wee's Big Adventure");
  expect(rankedMovies[2].title).toBe("The Big Green");
  expect(rankedMovies[3].title).toBe("Big");
  expect(rankedMovies[4].title).toBe("Big Hero 6");
});

test("rank movies - user 2 - likes newer movies", async () => {
  const userPreferences = new UserPreferences({
    prefersOlderMovies: false,
    genreRatings: {
      Action: 1,
      Comedy: 2,
      Drama: 1,
      Crime: 1,
      Animation: 5,
      Family: 4,
      Horror: 2,
      Fantasy: 3,
    },
    alreadyWatchedTitles: ["Pee-wee's Big Adventure"],
  });

  const movies = await api.fetchMovies();
  const rankedMovies = movieRanker.rankMovies(movies, userPreferences);

  expect(rankedMovies.length).toBe(7);

  const movieTitlesInRanking = rankedMovies.map((movie) => movie.title);
  expect(movieTitlesInRanking).not.toEqual(
    expect.arrayContaining(userPreferences.alreadyWatchedTitles)
  );

  expect(rankedMovies[0].title).toBe("Big Hero 6");
  expect(rankedMovies[1].title).toBe("Big");
  expect(rankedMovies[2].title).toBe("The Big Green");
  expect(rankedMovies[3].title).toBe("Big Fish");
  expect(rankedMovies[4].title).toBe("Big Lebowski");
  expect(rankedMovies[5].title).toBe("The Big Short");
  expect(rankedMovies[6].title).toBe("Big Daddy");
});
