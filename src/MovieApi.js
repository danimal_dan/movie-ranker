import { Movie } from "./model/Movie";

export class Api {
  /**
   * @returns Movie[]
   */
  fetchMovies() {
    const movies = [
      new Movie({
        title: "Big Lebowski",
        genres: "Comedy, Crime",
        rating: 8.1,
        released: 1998,
      }),
      new Movie({
        title: "Big Hero 6",
        genres: "Animation, Action, Adventure",
        rating: 7.8,
        released: 2014,
      }),
      new Movie({
        title: "Big Fish",
        genres: "Adventure, Drama, Fantasy",
        rating: 8.0,
        released: 2004,
      }),
      new Movie({
        title: "Big",
        genres: "Comedy, Drama, Fantasy",
        rating: 7.3,
        released: 1988,
      }),
      new Movie({
        title: "Big Daddy",
        genres: "Comedy, Drama",
        rating: 6.4,
        released: 1999,
      }),
      new Movie({
        title: "The Big Short",
        genres: "Biography, Comedy, Drama",
        rating: 7.8,
        released: 2015,
      }),
      new Movie({
        title: "Pee-wee's Big Adventure",
        genres: "Adventure, Comedy, Family",
        rating: 7.0,
        released: 1985,
      }),
      new Movie({
        title: "The Big Green",
        genres: "Family, Comedy, Sport",
        rating: 5.5,
        released: 1995,
      }),
    ];

    return movies;
  }
}

export default new Api();
