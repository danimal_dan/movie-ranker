import { Movie } from "./model/Movie";
import { UserPreferences } from "./model/UserPreferences";

export class MovieRanker {
  /**
   * Rank a list of movies based on a user's preferences. Ranking is based on a point system:
   *  - If user prefers older movies, and movie is 15 years old or older, give 5 points
   *  - Award point value for each matching genre in userPreferences.genreRatings
   *  - Tie breaker is the imdb rating of the movie
   *
   * This method also removes any movies the user has already watched.
   *
   * @param {Movie[]} unrankedMovies
   * @param {UserPreferences} userPreferences
   * @returns {Movie[]}
   */
  rankMovies(unrankedMovies, userPreferences) {
    throw new Error("not yet implemented");
  }
}

export default new MovieRanker();
