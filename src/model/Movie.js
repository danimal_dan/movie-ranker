export class Movie {
  /**
   * @type {string} Title of Movie
   */
  title;
  /**
   * @type {string} Comma separated list of genres
   */
  genres;
  /**
   * @type {number} IMDB Rating 0-10
   */
  rating;
  /**
   * @type {number} 4-digit release year
   */
  released;

  /**
   * @param {Movie} movie
   */
  constructor(movie) {
    if (movie) {
      this.title = movie.title;
      this.genres = movie.genres;
      this.rating = movie.rating;
      this.released = movie.released;
    }
  }
}
