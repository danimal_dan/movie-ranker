export class UserPreferences {
  /**
   * @type {boolean} User prefers movies 15 years old or older
   */
  prefersOlderMovies;
  /**
   * @type {object} Key value map, key is genre name, value is score
   */
  genreRatings;
  /**
   * @type {string[]} List of movies already seen by user
   */
  alreadyWatchedTitles;

  /**
   *
   * @param {UserPreferences} preferences
   */
  constructor(preferences) {
    if (preferences) {
      this.prefersOlderMovies = preferences.prefersOlderMovies ?? false;
      this.genreRatings = preferences.genreRatings ?? {};
      this.alreadyWatchedTitles = preferences.alreadyWatchedTitles ?? [];
    }
  }
}
